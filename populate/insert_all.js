const Answer = require("../models/Answers");
const QuestionAnswer = require("../models/QuestionAnswers");
const Question = require("../models/Questions");
const QuestionType = require("../models/QuestionTypes");
const Quiz = require("../models/Quiz");
const QuizQuestion = require("../models/QuizQuestions");
const Role = require("../models/Roles");
const Topic = require("../models/Topics");
const User = require("../models/Users");
const UserQuizStat = require("../models/UserQuizStats");
//1
const answersArr = [{
    answerName: 'anii 40 ai sec.XX'
  },
  {
    answerName: 'secolul XIX.'
  },
  {
    answerName: 'nu se ştie cind a apărut'
  },
  {
    answerName: 'anii 60 ai sec.XX'
  },
  {
    answerName: 'un calculator ce oferă o legatură tranzitivă în reţea'
  },
  {
    answerName: 'firma specializată ce oferă sevicii de conectare la Internet'
  },
  {
    answerName: 'o persoană care asigură conectarea la Internet'
  },
  {
    answerName: 'program client de conectare la reţea'
  },
  {
    answerName: 'Caracteristicile tehnice ale calculatorului'
  },
  {
    answerName: 'Calitatea liniei de conectare'
  },
  {
    answerName: 'Tipul conectării'
  },
  {
    answerName: 'Toate opţiunile de mai sus'
  },
  {
    answerName: 'compania Microsoft'
  },
  {
    answerName: 'are un guvern propriu'
  },
  {
    answerName: 'nu are un organ de conducere'
  },
  {
    answerName: 'compania IBM'
  },
  {
    answerName: 'multimedia'
  },
  {
    answerName: 'hypertext'
  },
  {
    answerName: 'hyperlegatură'
  },
  {
    answerName: 'site web'
  },
  {
    answerName: 'un calculator care prestează servicii Internet'
  },
  {
    answerName: 'un calculator prin care utilizatorii au acces la Internet'
  },
  {
    answerName: 'un nod de comunicare'
  },
  {
    answerName: 'un canal de transfer de date'
  },
  {
    answerName: 'adresa fizică obişnuită: ţara, localitatea, strada, blocul'
  },
  {
    answerName: 'adresa unică numita IP adresă'
  },
  {
    answerName: 'prin denumirea unui site'
  },
  {
    answerName: 'succesiune numerică de 4 bytes în format numeric'
  },
  {
    answerName: 'nu este posibil de localizat un calculator'
  },
  {
    answerName: 'linie telefonică'
  },
  {
    answerName: 'fibră optică'
  },
  {
    answerName: 'satelit'
  },
  {
    answerName: 'cablu coaxial'
  },
  {
    answerName: 'un dispozitiv ce realizează legătura între 2 reţele'
  },
  {
    answerName: 'un set de reguli de transmitere a datelor între 2 calculatoare'
  },
  {
    answerName: 'un contract de conectare la Internet'
  },
  {
    answerName: 'un limbaj comun de transmitere a datelor între 2 calculatoare'
  },
  {
    answerName: 'un limbaj de programare'
  },
  {
    answerName: 'nu cunosc aşa termen'
  },
  {
    answerName: 'reţea locală de calculatoare'
  },
  {
    answerName: 'reţea globală de calculatoare'
  },
  {
    answerName: 'reţea de reţele de calculatoare'
  },
  {
    answerName: 'protocolul oficial al Internet-ului'
  },
  {
    answerName: 'un dispozitiv de transmitere a datelor'
  },
  {
    answerName: 'limbaj de programare în Internet'
  },
  {
    answerName: 'nu cunosc aşa noţiune'
  },
  {
    answerName: 'un limbaj de programare'
  },
  {
    answerName: 'o reţea ARPA'
  },
  {
    answerName: 'o reţea zonală din SUA'
  },
  {
    answerName: 'o reţea DARPA'
  },
  {
    answerName: 'o reţea locală in SUA'
  },
  {
    answerName: 'o reţea de calculatoare'
  },
  {
    answerName: 'hard discul'
  },
  {
    answerName: 'discheta'
  },
  {
    answerName: 'discul optic'
  },
  {
    answerName: 'un calculator prin care utilizatorii au acces la Internet'
  },
  {
    answerName: 'un canal de transmitere de date'
  },
  {
    answerName: 'un nod de comunicare'
  },
  {
    answerName: 'un calculator care prestează servicii Internet'
  },
  {
    answerName: 'monitorul'
  },
  {
    answerName: 'scanerul'
  },
  {
    answerName: 'modemul'
  },
  {
    answerName: 'mouse-ul'
  },
  {
    answerName: 'microunde'
  },
  {
    answerName: 'linia telefonică'
  },
  {
    answerName: 'analitică'
  },
  {
    answerName: 'de pachete'
  },
  {
    answerName: 'calculatoare client şi server'
  },
  {
    answerName: 'imprimante'
  },
  {
    answerName: 'noduri de comutatie'
  },
  {
    answerName: 'scanere'
  },
  {
    answerName: 'canale de transfer de date'
  },
  {
    answerName: 'un calculator minim cu 166 Mhz, 32 RAM'
  },
  {
    answerName: 'un calculator performant de ultimă generaţie'
  },
  {
    answerName: 'orice calculator'
  },
  {
    answerName: 'test_answer1'
  },
  {
    answerName: 'test_answer2'
  },
  {
    answerName: 'test_answer3'
  },
  {
    answerName: 'test_answer4'
  },
  {
    answerName: 'test_answer5'
  },
  {
    answerName: 'test_answer6'
  },
  {
    answerName: 'test_answer7'
  },
  {
    answerName: 'test_answer8'
  },
  {
    answerName: 'test_answer9'
  },
  {
    answerName: 'test_answer10'
  },
  {
    answerName: 'test_answer11'
  },
  {
    answerName: 'test_answer12'
  },
  {
    answerName: 'test_answer13'
  },
  {
    answerName: 'test_answer14'
  },
  {
    answerName: 'test_answer15'
  },
  {
    answerName: 'test_answer16'
  },
  {
    answerName: 'test_answer17'
  },
  {
    answerName: 'test_answer18'
  },
  {
    answerName: 'test_answer19'
  },
  {
    answerName: 'test_answer20'
  },
  {
    answerName: 'test_answer21'
  },
  {
    answerName: 'test_answer22'
  },
  {
    answerName: 'test_answer23'
  },
  {
    answerName: 'test_answer24'
  },
  {
    answerName: 'test_answer25'
  },
  {
    answerName: 'test_answer1'
  },
  {
    answerName: 'test_answer2'
  },
  {
    answerName: 'test_answer3'
  },
  {
    answerName: 'test_answer4'
  },
  {
    answerName: 'test_answer5'
  },
  {
    answerName: 'test_answer6'
  },
  {
    answerName: 'test_answer7'
  },
  {
    answerName: 'test_answer8'
  },
  {
    answerName: 'test_answer9'
  },
  {
    answerName: 'test_answer10'
  },
  {
    answerName: 'test_answer11'
  },
  {
    answerName: 'test_answer12'
  },
  {
    answerName: 'test_answer13'
  },
  {
    answerName: 'test_answer14'
  },
  {
    answerName: 'test_answer15'
  },
  {
    answerName: 'test_answer16'
  },
  {
    answerName: 'test_answer17'
  },
  {
    answerName: 'test_answer18'
  },
  {
    answerName: 'test_answer19'
  },
  {
    answerName: 'test_answer20'
  },
  {
    answerName: 'test_answer21'
  },
  {
    answerName: 'test_answer22'
  },
  {
    answerName: 'test_answer23'
  },
  {
    answerName: 'test_answer24'
  },
  {
    answerName: 'test_answer25'
  }
];
//2
const questionAnswersArr = [{

    isCorrect: 0
  }, {

    isCorrect: 0
  }, {

    isCorrect: 0
  }, {

    isCorrect: 1
  },
  {
    isCorrect: 0
  }, {
    isCorrect: 1
  }, {
    isCorrect: 0
  }, {
    isCorrect: 0
  },
  {
    isCorrect: 0
  }, {
    isCorrect: 0
  }, {
    isCorrect: 0
  }, {
    isCorrect: 1
  },
  {
    isCorrect: 0
  }, {
    isCorrect: 0
  }, {
    isCorrect: 1
  }, {
    isCorrect: 0
  },
  {
    isCorrect: 0
  }, {
    isCorrect: 1
  }, {
    isCorrect: 0
  }, {
    isCorrect: 0
  },
  {
    isCorrect: 1
  }, {
    isCorrect: 0
  }, {
    isCorrect: 0
  }, {
    isCorrect: 0
  },
  {
    isCorrect: 0
  }, {
    isCorrect: 1
  }, {
    isCorrect: 0
  }, {
    isCorrect: 1
  }, {
    isCorrect: 0
  },
  {
    isCorrect: 0
  }, {
    isCorrect: 0
  }, {
    isCorrect: 1
  }, {
    isCorrect: 0
  },
  {
    isCorrect: 0
  }, {
    isCorrect: 1
  }, {
    isCorrect: 0
  }, {
    isCorrect: 1
  }, {
    isCorrect: 0
  },
  {
    isCorrect: 0
  }, {
    isCorrect: 0
  }, {
    isCorrect: 1
  }, {
    isCorrect: 1
  },
  {
    isCorrect: 1
  }, {
    isCorrect: 0
  }, {
    isCorrect: 0
  }, {
    isCorrect: 0
  }, {
    isCorrect: 0
  },
  {
    isCorrect: 1
  }, {
    isCorrect: 0
  }, {
    isCorrect: 1
  }, {
    isCorrect: 0
  },
  {
    isCorrect: 1
  }, {
    isCorrect: 0
  }, {
    isCorrect: 0
  }, {
    isCorrect: 0
  },
  {
    isCorrect: 1
  }, {
    isCorrect: 0
  }, {
    isCorrect: 0
  }, {
    isCorrect: 0
  },
  {
    isCorrect: 0
  }, {
    isCorrect: 0
  }, {
    isCorrect: 1
  }, {
    isCorrect: 0
  },
  {
    isCorrect: 0
  }, {
    isCorrect: 0
  }, {
    isCorrect: 0
  }, {
    isCorrect: 1
  },
  {
    isCorrect: 0
  }, {
    isCorrect: 1
  },
  {
    isCorrect: 1
  }, {
    isCorrect: 0
  }, {
    isCorrect: 1
  }, {
    isCorrect: 0
  }, {
    isCorrect: 1
  },
  {
    isCorrect: 1
  }, {
    isCorrect: 0
  }, {
    isCorrect: 0
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 0
  },
  {
    isCorrect: 1
  },
  {
    isCorrect: 0
  }
];
//3
const questionsArr = [{
    questionName: 'Internetul a apărut în:',
    points: 2
  },
  {
    questionName: 'Un ISP {Internet Service Provider} este:',
    points: 1
  },
  {
    questionName: 'Calitatea serviciilor oferite de o reţea de calculatoare depinde de:',
    points: 2
  },
  {
    questionName: 'Cine guvernează reţeaua Internet?',
    points: 1
  },
  {
    questionName: 'Modul de organizare a informatiei pe Web se numeşte:',
    points: 2
  },
  {
    questionName: 'Un server Web este:',
    points: 2
  },
  {
    questionName: 'Localizarea unui calculator în Internet se face prin:',
    points: 2
  },
  {
    questionName: 'Cea mai eficientă metodă de a conecta reţelele dintre 2 continente este:',
    points: 2
  },
  {
    questionName: 'Protocolul este:',
    points: 2
  },
  {
    questionName: 'Ce este Internet-ul?',
    points: 1
  },
  {
    questionName: 'TCP/IP este:',
    points: 2
  },
  {
    questionName: 'Predecesorul Internetului a fost:',
    points: 3
  },
  {
    questionName: 'Pentru transmiterea a 7 Mb de informaţie de la un calculator la altul este mai bine de utilizat:',
    points: 2
  },
  {
    questionName: 'Un terminal {calculator client} este:',
    points: 2
  },
  {
    questionName: 'Dispozitivul de conectare a calculatoarelor la Internet este:',
    points: 2
  },
  {
    questionName: 'Cea mai eficientă legatura între calculatoarele unei reţele de calculatoare locale se realizează prin:',
    points: 2
  },
  {
    questionName: 'Fişierele într-o reţea de calculatoare se transmit în formă:',
    points: 21
  },
  {
    questionName: 'Care din lista de mai jos sunt componentele de bază ale Internetului?',
    points: 3
  },
  {
    questionName: 'Calculatorul de pe care dorim să ne conectăm la Internet poate fi:',
    points: 2
  },
  {
    questionName: 'Item1',
    points: 2
  },
  {
    questionName: 'Item2',
    points: 1
  },
  {
    questionName: 'Item3',
    points: 2
  },
  {
    questionName: 'Item4',
    points: 3
  },
  {
    questionName: 'Item5',
    points: 2
  },
  {
    questionName: 'Item6',
    points: 2
  },
  {
    questionName: 'Item7',
    points: 3
  },
  {
    questionName: 'Item8',
    points: 2
  },
  {
    questionName: 'Item9',
    points: 2
  },
  {
    questionName: 'Item10',
    points: 1
  },
  {
    questionName: 'Item11',
    points: 1
  },
  {
    questionName: 'Item12',
    points: 2
  },
  {
    questionName: 'Item13',
    points: 2
  },
  {
    questionName: 'Item14',
    points: 2
  },
  {
    questionName: 'Item15',
    points: 2
  },
  {
    questionName: 'Item16',
    points: 1
  },
  {
    questionName: 'Item17',
    points: 2
  },
  {
    questionName: 'Item18',
    points: 2
  }
];
//4
const questionTypesArr = [{
    quesionTypeName: 'radio'
  },
  {
    quesionTypeName: 'checkbox'
  },
  {
    quesionTypeName: 'write'
  },
  {
    quesionTypeName: 'truefalse'
  },
  {
    quesionTypeName: 'dropdown'
  }
];
//5
const quizArr = [{
    quizName: 'first-quiz',
    quizPassword: '$2a$04$UV/ThrMOb5OAb9dyPtImVOHwBQRgm0kiJaYhINaNk3pWgCdjam4ke',
    randomShuffle: 1,
    timeLimit: 120
  },
  {
    quizName: 'second-quiz',
    quizPassword: '$2a$04$ScHeRxIF2ijVQ7mBufN0B.9.Jf/.bsIiubfpNkqSG7nWJRApdoWoe',
    randomShuffle: 0,
    timeLimit: 240
  },
  {
    quizName: 'test-quiz-topic-1',
    quizPassword: '$2a$04$BCOoXHVlyOVkWFPNUZ85GeBkwZXtKHxU/uS820X2/XNRWwn5SQdii',
    randomShuffle: 1,
    timeLimit: 350
  },
  {
    quizName: 'test-quiz2-topic-1',
    quizPassword: '$2a$04$wa6RL2CJSVhIvPETlzB2Vu.nvThmCQlaL5zrz7l9hSO.jhnk4KXzS',
    randomShuffle: 0,
    timeLimit: 530
  },
  {
    quizName: 'test-quiz3-topic-1',
    quizPassword: '$2a$04$mYoLBa50LeoQy7elzZ6iGeC2zNjFuvE4Vv7RcffvZIB/WdiDJIT.S',
    randomShuffle: 0,
    timeLimit: 110
  }

];
//6
const quizQuestionsArr = [{}];
//7
const rolesArr = [{
  roleName: "admin",
}, {
  roleName: "master",
}, {
  roleName: "guest",
}];
//8
const userQuizStatsArr = [{
  score: 16,
  solvedCorrect: 10,
  solvedIncorrect: 11,
}];
//9
const topicsArr = [{
    topicName: 'Comunitatea de rețele Internet',
    knowledgeOf: 'Retele de calculatoare'
  },
  {
    topicName: 'Structura documentului HTML. Formatarea caracterelor',
    knowledgeOf: 'HTML'
  },
  {
    topicName: 'Hypertext. Noțiuni și concepte de bazã.',
    knowledgeOf: 'HTML'
  },
  {
    topicName: 'Liste. Noțiuni și Marcaje utilizate.',
    knowledgeOf: 'HTML'
  },
  {
    topicName: 'Tabele. Noțiuni și Marcaje utilizate.',
    knowledgeOf: 'HTML'
  },
  {
    topicName: 'Operarea cu obiecte. Inserarea imaginilor, sunetelor și videoclipurilor.',
    knowledgeOf: 'HTML'
  },
  {
    topicName: 'Referințe. Referințe interne și externe.',
    knowledgeOf: 'HTML'
  },
  {
    topicName: 'Operarea cu formulare. Marcaje utilizate.',
    knowledgeOf: 'HTML'
  },
  {
    topicName: 'Ferestre în HTML. Comenzi de construire a ferestrelor.',
    knowledgeOf: 'HTML'
  },
  {
    topicName: 'Inițiere în CSS.',
    knowledgeOf: 'CSS'
  },
  {
    topicName: 'Stiluri CSS.',
    knowledgeOf: 'CSS'
  },
  {
    topicName: 'Interfața aplicației.',
    knowledgeOf: 'CSS'
  },
  {
    topicName: 'Aplicația Dreamweaver.',
    knowledgeOf: 'CSS'
  },
  {
    topicName: 'Limbajul PHP - inițiere.',
    knowledgeOf: 'PHP'
  },
  {
    topicName: 'Limbajul PHP - mediu.',
    knowledgeOf: 'PHP'
  },
  {
    topicName: 'Limbajul PHP - avansat.',
    knowledgeOf: 'PHP'
  },
  {
    topicName: 'Limbajul Javascript - inițiere.',
    knowledgeOf: 'Javascript'
  }
];
//10
const usersArr = [{
  userName: "dbmaster",
  userPassword: '$2a$04$0MaUGArY5R/hgTlFriR0xu8pNf/NbXnBD5CGqKfdiF0w711QVWg/i'
}, {
  userName: 'dbadmin',
  userPassword: '$2a$04$SxJ8W5T/Lw7qsGOls8x0Zu53NoihHkjE0IsT/8K2YT.2pZWT4LH1.'
}];

async function createInstances() {

  //console.log("Works!")
  //CREATE INSTANCES
  //1
  answersArr.forEach((element, index, theArr) => {
    theArr[index] = new Answer(element)
  });
  //2
  questionAnswersArr.forEach((element, index, theArr) => {
    theArr[index] = new QuestionAnswer(element)
  });

  //3
  questionsArr.forEach((element, index, theArr) => {
    theArr[index] = new Question(element)
  });

  //4
  questionTypesArr.forEach((element, index, theArr) => {
    theArr[index] = new QuestionType(element)
  });
  //5
  quizArr.forEach((element, index, theArr) => {
    theArr[index] = new Quiz(element)
  });

  //6
  quizQuestionsArr.forEach((element, index, theArr) => {
    theArr[index] = new QuizQuestion(element)
  });

  //7
  rolesArr.forEach((element, index, theArr) => {
    theArr[index] = new Role(element)
  });
  //8
  topicsArr.forEach((element, index, theArr) => {
    theArr[index] = new Topic(element);
    //console.log(theArr[index]);
  });
  //console.log(topicsArr);
  //9
  userQuizStatsArr.forEach((element, index, theArr) => {
    theArr[index] = new UserQuizStat(element)
  });
  //10
  usersArr.forEach((element, index, theArr) => {
    theArr[index] = new User(element)
  });
};
async function pushRefs() {
  //console.log(topicsArr);
  // console.log(rolesArr);
  // console.log(questionTypesArr);

  //Users go to Roles
  rolesArr.forEach((element, index, theArr) => {
    theArr[index].usersIDs.push(usersArr[0]);
    //console.log(theArr[index]);
  });

  //Questions go to QuestionTypes
  questionTypesArr.forEach((element, index, theArr) => {
    theArr[index].questionsIDs.push(questionsArr[0]);
    console.log(theArr[index]);
  });

  //Quizes go to Topics
  topicsArr.forEach((element, index, theArr) => {
    theArr[index].quizesIDs.push(quizArr[0]);
    //console.log(theArr[index]);
  });

  // __QUIZ QUESTIONS__
  //send an entire object
  quizQuestionsArr[0].quizID = quizArr[0];
  quizQuestionsArr[0].questionID = questionsArr[0];

  //__QUESTION ANSWERS__
  questionAnswersArr[0].questionID = questionsArr[0];
  questionAnswersArr[0].answerID = answersArr[0];

  //__USER QUIZ STATS 
  userQuizStatsArr[0].quizID = quizArr[0];
  userQuizStatsArr[0].userID = usersArr[0];
}
async function saveInstances() {

  //SAVE
  //save as promises
  //1
  answersArr.forEach((element, index, theArr) => {
    theArr[index] = element.save();
  });
  //2
  questionAnswersArr.forEach((element, index, theArr) => {
    theArr[index] = element.save();
  });

  //3
  questionsArr.forEach((element, index, theArr) => {
    theArr[index] = element.save();
  });

  //4
  questionTypesArr.forEach((element, index, theArr) => {
    theArr[index] = element.save();
  });
  //5
  quizArr.forEach((element, index, theArr) => {
    theArr[index] = element.save();
  });

  //6
  quizQuestionsArr.forEach((element, index, theArr) => {
    theArr[index] = element.save();
  });

  //7
  rolesArr.forEach((element, index, theArr) => {
    theArr[index] = element.save();
  });
  //8
  topicsArr.forEach((element, index, theArr) => {
    theArr[index] = element.save();
  });
  //9
  userQuizStatsArr.forEach((element, index, theArr) => {
    theArr[index] = element.save();
  });
  //10
  usersArr.forEach((element, index, theArr) => {
    theArr[index] = element.save();
  });
}
async function executeQuery() {
  //AWAIT THE CREATION
  await createInstances();
  console.log("Before push");
  //PUSH EVERYTHING
  await pushRefs();
  console.log("Before save");
  await saveInstances();
  console.log("After save");
}

module.exports = {

  
  insertData(req, res, next) {
    let allPromises = [];
    executeQuery();
    //THE ORDER MATTERS
    allPromises = [...rolesArr, ...usersArr, ...questionTypesArr, ...questionsArr, ...topicsArr, ...quizArr, ...answersArr, ...questionAnswersArr,
      ...quizQuestionsArr, ...userQuizStatsArr
    ];

    console.log("done init save");
    //COMMIT 
    Promise.all(allPromises)
      .then(data => {
        res.status(200).send(data);
      })
      // })
      .catch(next);
  }

}
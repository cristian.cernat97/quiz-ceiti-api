const Answer = require("../models/Answers");
const QuestionAnswer = require("../models/QuestionAnswers");
const Question = require("../models/Questions");
const QuestionType = require("../models/QuestionTypes");
const Quiz = require("../models/Quiz");
const QuizQuestion = require("../models/QuizQuestions");
const Role = require("../models/Roles");
const Topic = require("../models/Topics");
const User = require("../models/Users");
const UserQuizStat = require("../models/UserQuizStats");

module.exports = {
    populateQuestionTypes(req, res, next) {
        QuestionType.find({})
            .populate({
                path: "questionsIDs",
                model: "Questions"
            })
            .then((data) => {
                res.status(200).send(data);
            }).catch(next);;
    },
    populateQuestionsAnswers(req, res, next) {
        QuestionAnswer.find()
            .populate({
                path: "answerID",
                model: "Answers"
            })
            .populate({
                path: "questionID",
                model: "Questions"
            })
            .then((data) => {
                res.status(200).send(data);
            }).catch(next);;
    },
    populateTopics(req, res, next) {
        Topic.find({})
            .populate({
                path: "quizesIDs",
                model: "Quiz"
            }).then((data) => {
                res.status(200).send(data);
            }).catch(next);
    },
    populateRoles(req, res, next) {
        Role.find({})
            .populate({
                path: "usersIDs",
                model: "Users"
            }).then((data) => {
                res.status(200).send(data);
            }).catch(next);
    },
    populateQuizQuestions(req, res, next) {
        QuizQuestion.find()
            .populate({
                path: "quizID",
                model: "Quiz"
            })
            .populate({
                path: "questionID",
                model: "Questions"
            }).then((data) => {
                res.status(200).send(data);
            }).catch(next);
    },
    populateUserQuizStats(req, res, next) {
        UserQuizStat.find({})
            .populate({
                path: "quizID",
                model: "Quiz"
            })
            .populate({
                path: "userID",
                model: "Users"
            }).then((data) => {
                res.status(200).send(data);
            }).catch(next);
    }

};
const questionAnswer = require("../models/QuestionAnswers");
module.exports = {
    greeting(req, res) {
        res.send({
            hi: "questionAnswers greet you"
        })
    },
    create(req, res, next) {
        console.log(req.body);
        const questionAnswerProps = req.body;
        questionAnswer.create(questionAnswerProps)
            .then(questionAnswer =>
                res.status(200).send(questionAnswer)
            )
            .catch(next) //next middleware in chain
    },
    read(req, res, next) {
        questionAnswer.find({}).then((questionAnswers) => {
                res.status(200).send(questionAnswers)
            })
            .catch(next)
    },

    update(req, res, next) {
        const questionAnswerId = req.params.id;
        const questionAnswerProps = req.body;

        questionAnswer.findByIdAndUpdate({
                _id: questionAnswerId
            }, questionAnswerProps)
            .then(() => {
                questionAnswer.findById({
                        _id: questionAnswerId
                    })
                    .then(questionAnswer => res.status(200).send(questionAnswer))
                    .catch(next)
            })
    },

    delete(req, res, next) {
        const questionAnswerId = req.params.id;
        const questionAnswerProps = req.body;

        questionAnswer.findByIdAndRemove({
                _id: questionAnswerId
            })
            .then(questionAnswer => res.status(204).send(questionAnswer))
            //204 stands for succes
            .catch(next)
    }
}
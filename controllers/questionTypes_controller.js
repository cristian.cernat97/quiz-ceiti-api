const questionType = require("../models/QuestionTypes");
module.exports = {
    greeting(req, res) {
        res.send({
            hi: "questionTypes greet you"
        })
    },
    create(req, res, next) {
        console.log(req.body);
        const questionTypeProps = req.body;
        questionType.create(questionTypeProps)
            .then(questionType =>
                res.status(200).send(questionType)
            )
            .catch(next) //next middleware in chain
    },
    read(req, res, next) {
        questionType.find({}).then((questionTypes) => {
                res.status(200).send(questionTypes)
            })
            .catch(next)
    },

    update(req, res, next) {
        const questionTypeId = req.params.id;
        const questionTypeProps = req.body;

        questionType.findByIdAndUpdate({
                _id: questionTypeId
            }, questionTypeProps)
            .then(() => {
                questionType.findById({
                        _id: questionTypeId
                    })
                    .then(questionType => res.status(200).send(questionType))
                    .catch(next)
            })
    },

    delete(req, res, next) {
        const questionTypeId = req.params.id;
        const questionTypeProps = req.body;

        questionType.findByIdAndRemove({
                _id: questionTypeId
            })
            .then(questionType => res.status(204).send(questionType))
            //204 stands for succes
            .catch(next)
    }
}
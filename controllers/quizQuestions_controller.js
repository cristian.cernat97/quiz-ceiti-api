const quizQuestion = require("../models/QuizQuestions");
module.exports = {
    greeting(req, res) {
        res.send({
            hi: "quizQuestions greet you"
        })
    },
    create(req, res, next) {
        console.log(req.body);
        const quizQuestionProps = req.body;
        quizQuestion.create(quizQuestionProps)
            .then(quizQuestion =>
                res.status(200).send(quizQuestion)
            )
            .catch(next) //next middleware in chain
    },
    read(req, res, next) {
        quizQuestion.find({}).then((quizQuestions) => {
                res.status(200).send(quizQuestions)
            })
            .catch(next)
    },

    update(req, res, next) {
        const quizQuestionId = req.params.id;
        const quizQuestionProps = req.body;

        quizQuestion.findByIdAndUpdate({
                _id: quizQuestionId
            }, quizQuestionProps)
            .then(() => {
                quizQuestion.findById({
                        _id: quizQuestionId
                    })
                    .then(quizQuestion => res.status(200).send(quizQuestion))
                    .catch(next)
            })
    },

    delete(req, res, next) {
        const quizQuestionId = req.params.id;
        const quizQuestionProps = req.body;

        quizQuestion.findByIdAndRemove({
                _id: quizQuestionId
            })
            .then(quizQuestion => res.status(204).send(quizQuestion))
            //204 stands for succes
            .catch(next)
    }
}
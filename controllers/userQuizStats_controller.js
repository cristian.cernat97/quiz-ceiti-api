const userQuizStat = require("../models/UserQuizStats");

module.exports = {
    greeting(req, res) {
        res.send({
            hi: "userQuizStats greet you"
        })
    },
    create(req, res, next) {
        console.log(req.body);
        const userQuizStatProps = req.body;
        userQuizStat.create(userQuizStatProps)
            .then(userQuizStat =>
                res.status(200).send(userQuizStat)
            )
            .catch(next) //next middleware in chain
    },
    read(req, res, next) {
        userQuizStat.find({}).then((userQuizStats) => {
                res.status(200).send(userQuizStats)
            })
            .catch(next)
    },

    update(req, res, next) {
        const userQuizStatId = req.params.id;
        const userQuizStatProps = req.body;

        userQuizStat.findByIdAndUpdate({
                _id: userQuizStatId
            }, userQuizStatProps)
            .then(() => {
                userQuizStat.findById({
                        _id: userQuizStatId
                    })
                    .then(userQuizStat => res.status(200).send(userQuizStat))
                    .catch(next)
            })
    },

    delete(req, res, next) {
        const userQuizStatId = req.params.id;
        const userQuizStatProps = req.body;

        userQuizStat.findByIdAndRemove({
                _id: userQuizStatId
            })
            .then(userQuizStat => res.status(204).send(userQuizStat))
            //204 stands for succes
            .catch(next)
    }
}
const Driver = require("../models/driver");
module.exports = {
    greeting(req, res) {
        res.send({
            hi: "there"
        })
    },
    //geography
    index(req, res, next) {
        // ex http://google.com?lng=80&lat=60
        const {
            lng,
            lat
        } = req.query;

        Driver.find({
                'geometry.coordinates': {
                    $nearSphere: {
                        $geometry: {
                            type: "Point",
                            coordinates: [lng, lat]
                        },
                        $maxDistance: 200000
                    }
                }
            })
            .then(drivers => res.send(drivers))
            .catch(next);
    },
    create(req, res, next) {
        console.log(req.body);
        const driverProps = req.body;
        Driver.create(driverProps)
            .then(driver =>
                res.send(driver)
            )
            .catch(next) //next middleware in chain
    },
    edit(req, res, next) {
        const driverId = req.params.id;
        const driverProps = req.body;

        Driver.findByIdAndUpdate({
                _id: driverId
            }, driverProps)
            .then(() => {
                Driver.findById({
                        _id: driverId
                    })
                    .then(driver => res.send(driver))
                    .catch(next)
            })
    },

    delete(req, res, next) {
        const driverId = req.params.id;
        const driverProps = req.body;

        Driver.findByIdAndRemove({
                _id: driverId
            })
            .then(driver => res.status(204).send(driver))
            //204 stands for succes
            .catch(next)
    }
}
const quiz = require("../models/Quiz");
module.exports = {
    greeting(req, res) {
        res.send({
            hi: "quizes greet you"
        })
    },
    create(req, res, next) {
        console.log(req.body);
        const quizProps = req.body;
        quiz.create(quizProps)
            .then(quiz =>
                res.status(200).send(quiz)
            )
            .catch(next) //next middleware in chain
    },
    read(req, res, next) {
        quiz.find({}).then((quizs) => {
                res.status(200).send(quizs)
            })
            .catch(next)
    },

    update(req, res, next) {
        const quizId = req.params.id;
        const quizProps = req.body;

        quiz.findByIdAndUpdate({
                _id: quizId
            }, quizProps)
            .then(() => {
                quiz.findById({
                        _id: quizId
                    })
                    .then(quiz => res.status(200).send(quiz))
                    .catch(next)
            })
    },

    delete(req, res, next) {
        const quizId = req.params.id;
        const quizProps = req.body;

        quiz.findByIdAndRemove({
                _id: quizId
            })
            .then(quiz => res.status(204).send(quiz))
            //204 stands for succes
            .catch(next)
    }
}
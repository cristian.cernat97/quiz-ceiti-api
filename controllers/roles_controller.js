const role = require("../models/Roles");
module.exports = {
    greeting(req, res) {
        res.send({
            hi: "Roles greet you"
        })
    },
    create(req, res, next) {
        console.log(req.body);
        const roleProps = req.body;
        role.create(roleProps)
            .then(role =>
                res.status(200).send(role)
            )
            .catch(next) //next middleware in chain
    },
    read(req, res, next) {
        role.find({}).then((roles) => {
                res.status(200).send(roles)
            })
            .catch(next)
    },

    update(req, res, next) {
        const roleId = req.params.id;
        const roleProps = req.body;

        role.findByIdAndUpdate({
                _id: roleId
            }, roleProps)
            .then(() => {
                role.findById({
                        _id: roleId
                    })
                    .then(role => res.status(200).send(role))
                    .catch(next)
            })
    },

    delete(req, res, next) {
        const roleId = req.params.id;
        const roleProps = req.body;

        role.findByIdAndRemove({
                _id: roleId
            })
            .then(role => res.status(204).send(role))
            //204 stands for succes
            .catch(next)
    }
}
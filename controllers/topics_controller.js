const topic = require("../models/Topics");
module.exports = {
    greeting(req, res) {
        res.send({
            hi: "topics greet you"
        })
    },
    create(req, res, next) {
        console.log(req.body);
        const topicProps = req.body;
        topic.create(topicProps)
            .then(topic =>
                res.status(200).send(topic)
            )
            .catch(next) //next middleware in chain
    },
    read(req, res, next) {
        topic.find({}).then((topics) => {
                res.status(200).send(topics)
            })
            .catch(next)
    },

    update(req, res, next) {
        const topicId = req.params.id;
        const topicProps = req.body;

        topic.findByIdAndUpdate({
                _id: topicId
            }, topicProps)
            .then(() => {
                topic.findById({
                        _id: topicId
                    })
                    .then(topic => res.status(200).send(topic))
                    .catch(next)
            })
    },

    delete(req, res, next) {
        const topicId = req.params.id;
        const topicProps = req.body;

        topic.findByIdAndRemove({
                _id: topicId
            })
            .then(topic => res.status(204).send(topic))
            //204 stands for succes
            .catch(next)
    }
}
const question = require("../models/Questions");
module.exports = {
    greeting(req, res) {
        res.send({
            hi: "questions greet you"
        })
    },
    create(req, res, next) {
        console.log(req.body);
        const questionProps = req.body;
        question.create(questionProps)
            .then(question =>
                res.status(200).send(question)
            )
            .catch(next) //next middleware in chain
    },
    read(req, res, next) {
        question.find({}).then((questions) => {
                res.status(200).send(questions)
            })
            .catch(next)
    },

    update(req, res, next) {
        const questionId = req.params.id;
        const questionProps = req.body;

        question.findByIdAndUpdate({
                _id: questionId
            }, questionProps)
            .then(() => {
                question.findById({
                        _id: questionId
                    })
                    .then(question => res.status(200).send(question))
                    .catch(next)
            })
    },

    delete(req, res, next) {
        const questionId = req.params.id;
        const questionProps = req.body;

        question.findByIdAndRemove({
                _id: questionId
            })
            .then(question => res.status(204).send(question))
            //204 stands for succes
            .catch(next)
    }
}
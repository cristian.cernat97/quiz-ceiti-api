const answer = require("../models/Answers");
module.exports = {
    greeting(req, res) {
        res.send({
            hi: "answer greet you"
        })
    },
    create(req, res, next) {
        console.log(req.body);
        const answerProps = req.body;
        answer.create(answerProps)
            .then(answer =>
                res.status(200).send(answer)
            )
            .catch(next) //next middleware in chain
    },
    read(req, res, next) {
        answer.find({}).then((answer) => {
                res.status(200).send(answer)
            })
            .catch(next)
    },

    update(req, res, next) {
        const answerId = req.params.id;
        const answerProps = req.body;

        answer.findByIdAndUpdate({
                _id: answerId
            }, answerProps)
            .then(() => {
                answer.findById({
                        _id: answerId
                    })
                    .then(answer => res.status(200).send(answer))
                    .catch(next)
            })
    },

    delete(req, res, next) {
        const answerId = req.params.id;
        const answerProps = req.body;

        answer.findByIdAndRemove({
                _id: answerId
            })
            .then(answer => res.status(204).send(answer))
            //204 stands for succes
            .catch(next)
    }
}
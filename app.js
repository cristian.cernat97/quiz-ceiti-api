const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const routes = require('./routes/routes');
const app = express();

mongoose.Promise = global.Promise
//node env environment variable if not in test 
//test is separated in test/test_helper
if (process.env.NODE_ENV !== 'test') {
    mongoose.connect("mongodb+srv://airForce:Silvian@quiz-ektqc.mongodb.net/api_test")
        .then(() => {
            console.log('connection successful')
        })
        .catch((err) => console.error(err))
}
app.use(bodyParser.json());
routes(app);

app.use((err, req, res, next) => {
    res.status(422).send({
        err: err.message,
    });
})

module.exports = app;
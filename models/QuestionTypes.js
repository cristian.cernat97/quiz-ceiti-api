var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var QuestionTypesSchema = new Schema({
  quesionTypeName: {
    type: String,
    trim: true
  },

  questionsIDs: [{
    type: Schema.Types.ObjectId,
    ref: "Questions"
  }],
  updated_date: {
    type: Date,
    default: Date.now
  },
});

module.exports = mongoose.model("QuestionTypes", QuestionTypesSchema);
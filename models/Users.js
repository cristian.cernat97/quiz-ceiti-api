var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var UsersSchema = new Schema({
  userName: {
    type: String,
    trim: true
  },

  userPassword: String,
  // // --MANY
  // user_quiz_statsIDs: [{
  //   type: Schema.Types.ObjectId,
  //   ref: "UserQuizStats"
  // }],
  updated_date: {
    type: Date,
    default: Date.now
  },
});

module.exports = mongoose.model("Users", UsersSchema);
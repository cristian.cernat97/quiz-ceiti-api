var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var UserQuizStatsSchema = new Schema({
  mark: {
    type: Number,
    trim: true
  },
  score: {
    type: Number,
    trim: true
  },
  solvedCorrect: {
    type: Number,
    trim: true
  },
  solvedIncorrect: {
    type: Number,
    trim: true
  },
  skipped: {
    type: Number,
    trim: true
  },
  // ONE TO ONE
  quizID: {
    type: Schema.Types.ObjectId,
    ref: "Quiz"
  },
  //ONE TO ONE
  userID: {
    type: Schema.Types.ObjectId,
    ref: "Users"
  },
  updated_date: {
    type: Date,
    default: Date.now
  },
}, {
  autoIndex: false
});

module.exports = mongoose.model("UserQuizStats", UserQuizStatsSchema);
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var QuestionsSchema = new Schema({
  questionName: {
    type: String,
    trim: true
  },
  points: {
    type: Number,
    trim: true
  },
  // // --MANY
  // question_answersIDs: [{
  //   type: Schema.Types.ObjectId,
  //   ref: "QuestionAnswers"
  // }],
  // // --MANY
  // quiz_questionsIDs: [{
  //   type: Schema.Types.ObjectId,
  //   ref: "QuizQuestions"
  // }],
  updated_date: {
    type: Date,
    default: Date.now
  },
});


module.exports = mongoose.model("Questions", QuestionsSchema);
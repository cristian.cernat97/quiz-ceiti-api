var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var TopicsSchema = new Schema({

  topicName: {
    type: String,
    trim: true
  },
  knowledgeOf: {
    type: String,
    trim: true
  },
  // --MANY
  quizesIDs: [{
    type: Schema.Types.ObjectId,
    ref: "Quiz"
  }],
  updated_date: {
    type: Date,
    default: Date.now
  },
});

module.exports = mongoose.model("Topics", TopicsSchema);
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var QuestionAnswersSchema = new Schema({
  //ONE TO ONE
  answerID: {
    type: Schema.Types.ObjectId,
    ref: "Answers"
  },
  questionID: {
    type: Schema.Types.ObjectId,
    ref: "Questions"
  },
  isCorrect: Boolean,
  updated_date: {
    type: Date,
    default: Date.now
  },
}, {
  autoIndex: false
});

module.exports = mongoose.model("QuestionAnswers", QuestionAnswersSchema);
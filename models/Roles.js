var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var RolesSchema = new Schema({
  roleName: {
    type: String,
    trim: true
  },
  // --MANY
  usersIDs: [{
    type: Schema.Types.ObjectId,
    ref: "Users"
  }],
  updated_date: {
    type: Date,
    default: Date.now
  },
});

module.exports = mongoose.model("Roles", RolesSchema);

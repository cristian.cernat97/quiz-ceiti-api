var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var QuizSchema = new Schema({
  quizName: {
    type: String,
    trim: true
  },
  quizPassword: {
    type: String,
    trim: true
  },
  randomShuffle: Boolean,
  timeLimit: {
    type: Number,
    trim: true
  },
  // // --MANY
  // quiz_questionsIDs: [{
  //   type: Schema.Types.ObjectId,
  //   ref: "QuizQuestions"
  // }],
  updated_date: {
    type: Date,
    default: Date.now
  },
});

module.exports = mongoose.model("Quiz", QuizSchema);
var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var AnswersSchema = new Schema({
  answerName: {
    type: String,
    trim: true
  },
  // // --MANY
  // question_answersIDs: [{
  //   type: Schema.Types.ObjectId,
  //   ref: "QuestionAnswers"
  // }],
  updated_date: {
    type: Date,
    default: Date.now
  },
});

module.exports = mongoose.model("Answers", AnswersSchema, );
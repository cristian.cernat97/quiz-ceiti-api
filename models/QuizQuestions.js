var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var QuizQuestionsSchema = new Schema({
  quizID: {
    type: Schema.Types.ObjectId,
    ref: 'Quiz'
  },
  questionID: {
    type: Schema.Types.ObjectId,
    ref: 'Questions'
  },
  updated_date: {
    type: Date,
    default: Date.now
  },
}, {
  autoIndex: false
});

module.exports = mongoose.model("QuizQuestions", QuizQuestionsSchema);
const DriversController = require("../controllers/drivers_controller");

const AnswersController = require("../controllers/answers_controller");
const QuestionAnswersController = require("../controllers/questionAnswers_controller");
const QuestionsController = require("../controllers/questions_controller");
const QuestionTypesController = require("../controllers/questionTypes_controller");
const QuizController = require("../controllers/quiz_controller");
const RolesController = require("../controllers/roles_controller");
const TopicsController = require("../controllers/topics_controller");
const UserQuizStatsController = require("../controllers/userQuizStats_controller");
const UsersController = require("../controllers/users_controller");

const PopulateSubdocuments = require("../populate/populate_subdocuments");
const AllPopulate = require('../populate/insert_all');


module.exports = (app) => {
    //TEST ROUTES
    app.get('/api', DriversController.greeting);
    app.post('/api/drivers', DriversController.create);
    app.put('/api/drivers/:id', DriversController.edit)
    app.delete('/api/drivers/:id', DriversController.delete);
    app.get('/api/drivers', DriversController.index);


    //POPULATE
    app.get('/api/all/populate/insertData', AllPopulate.insertData);

    app.get('/api/all/populate/populateQuestionTypes', PopulateSubdocuments.populateQuestionTypes);
    app.get('/api/all/populate/populateQuestionsAnswers', PopulateSubdocuments.populateQuestionsAnswers);
    app.get('/api/all/populate/populateTopics', PopulateSubdocuments.populateTopics);
    app.get('/api/all/populate/populateRoles', PopulateSubdocuments.populateRoles);
    app.get('/api/all/populate/populateQuizQuestions', PopulateSubdocuments.populateQuizQuestions);
    app.get('/api/all/populate/populateUserQuizStats', PopulateSubdocuments.populateUserQuizStats);

    //ANSWERS ROUTES
    app.get('/api/answers/greet', AnswersController.greeting);
    app.get('/api/answers', AnswersController.read);
    app.post('/api/answers', AnswersController.create);
    app.put('/api/answers/:id', AnswersController.update)
    app.delete('/api/answers/:id', AnswersController.delete);

    //QUESTION ANSWERS ROUTES
    app.get('/api/questionAnswers/greet', QuestionAnswersController.greeting);
    app.get('/api/questionAnswers', QuestionAnswersController.read);
    app.post('/api/questionAnswers', QuestionAnswersController.create);
    app.put('/api/questionAnswers/:id', QuestionAnswersController.update)
    app.delete('/api/questionAnswers/:id', QuestionAnswersController.delete);

    //QUESTIONS ROUTES
    app.get('/api/questions/greet', QuestionsController.greeting);
    app.get('/api/questions', QuestionsController.read);
    app.post('/api/questions', QuestionsController.create);
    app.put('/api/questions/:id', QuestionsController.update)
    app.delete('/api/questions/:id', QuestionsController.delete);

    //QUESTION TYPES ROUTES
    app.get('/api/questionTypes/greet', QuestionTypesController.greeting);
    app.get('/api/questionTypes', QuestionTypesController.read);
    app.post('/api/questionTypes', QuestionTypesController.create);
    app.put('/api/questionTypes/:id', QuestionTypesController.update)
    app.delete('/api/questionTypes/:id', QuestionTypesController.delete);

    //QUIZES ROUTES
    app.get('/api/quiz/greet', QuizController.greeting);
    app.get('/api/quiz', QuizController.read);
    app.post('/api/quiz', QuizController.create);
    app.put('/api/quiz/:id', QuizController.update)
    app.delete('/api/quiz/:id', QuizController.delete);

    //ROLES ROUTES
    app.get('/api/roles/greet', RolesController.greeting);
    app.get('/api/roles', RolesController.read);
    app.post('/api/roles', RolesController.create);
    app.put('/api/roles/:id', RolesController.update)
    app.delete('/api/roles/:id', RolesController.delete);

    //TOPICS ROUTES
    app.get('/api/topics/greet', TopicsController.greeting);
    app.get('/api/topics', TopicsController.read);
    app.post('/api/topics', TopicsController.create);
    app.put('/api/topics/:id', TopicsController.update)
    app.delete('/api/topics/:id', TopicsController.delete);

    //USERQUIZ STATS ROUTES
    app.get('/api/userQuizStats/greet', UserQuizStatsController.greeting);
    app.get('/api/userQuizStats', UserQuizStatsController.read);
    app.post('/api/userQuizStats', UserQuizStatsController.create);
    app.put('/api/userQuizStats/:id', UserQuizStatsController.update)
    app.delete('/api/userQuizStats/:id', UserQuizStatsController.delete);

    //USERS ROUTES
    app.get('/api/users/greet', UsersController.greeting);
    app.get('/api/users', UsersController.read);
    app.post('/api/users', UsersController.create);
    app.put('/api/users/:id', UsersController.update)
    app.delete('/api/users/:id', UsersController.delete);

};
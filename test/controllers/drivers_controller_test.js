const assert = require('assert');
const request = require("supertest");
const mongoose = require("mongoose");
const app = require("../../app");
const Driver = mongoose.model("driver");
describe("Drivers controller", () => {

    it("Post to /api/drivers creates new driver", done => {

        Driver.count().then(count => {
            request(app) //fake request with supertest
                .post('/api/drivers')
                .send({
                    email: 'test@test.com',
                    driving: true
                })
                .end(() => {

                    Driver.count().then(newCount => {
                        console.log("!" + count);
                        assert(count + 1 === newCount);
                        console.log("!!" + newCount);
                        done();
                    });
                });
        });
    });


    it("Put to /api/drivers/id updates the driver", done => {
        const driver = new Driver({
            email: 't@t.com',
            driving: false
        });

        driver.save().then(() => {
            request(app)
                .put(`/api/drivers/${driver._id}`)
                .send({
                    driving: true
                })
                .end(() => {
                    Driver.findOne({
                            email: 't@t.com'
                        })
                        .then(driver => {
                            assert(driver.driving === true);
                            done();
                        });
                });
        });
    });
    it("Delete to /api/drivers/id  driver", done => {
        const driver = new Driver({
            email: 'test@t.com'
        });

        driver.save().then(() => {
            request(app)
                .delete(`/api/drivers/${driver._id}`)
                .end(() => { //end request
                    Driver.findOne({
                            email: 'test@t.com'
                        })
                        .then(driver => {
                            //console.log(driver)
                            assert(driver === null);
                            done();
                        });
                });
        });
    });

    it(" Get to /api/drivers find drivers in a location", done => {
        const seattleDriver = new Driver({
            email: 'seattle@t.com',
            geometry: {
                type: 'Point',
                coordinates: [-122.4759902, 47.6147628]
            }
        });

        const maiamiDriver = new Driver({
            email: 'maiami@t.com',
            geometry: {
                type: 'Point',
                coordinates: [-80.253, 25.791]
            }
        });

        Promise.all([seattleDriver.save(), maiamiDriver.save()])
            .then(() => {
                request(app)
                    .get(`/api/drivers/?lng=-80&lat=25`)
                    .end((err, response) => { //end request
                        //console.log(response);
                        assert(response.body.length === 1);
                        assert(response.body[0].email === 'maiami@t.com');
                        done();
                    });
            });
    });
});
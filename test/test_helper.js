const mongoose = require('mongoose');
before(done => {
    mongoose.connect('mongodb://localhost/muber_test');
    mongoose.connection
        .once('open', () => done())
        .on('error', err => {
            console.warn("Warning", err);
        })
})

beforeEach(function (done) { //for timeout
    this.timeout(10000);
    const {
        drivers
    } = mongoose.connection.collections;

    drivers.drop()
        .then(() => drivers.ensureIndex({
            'geometry.coordinates': '2dsphere'
        })) //geometry is recreated
        .then(() => done())
        .catch(() => done());
})
[![Known Vulnerabilities](https://snyk.io/test/github/{cristian.cernat97}/{quiz-ceiti-api}/badge.svg)](https://snyk.io/test/github/{cristian.cernat97}/{quiz-ceiti-api})
# Project Title

API made for the Quiz CEITI APP in order to get and receive different types of requests 


## Getting Started
Make sure you have installed:

* [Node](https://nodejs.org/en/) NodeJS
* [MongoDB] (https://www.mongodb.com/download-center#community) Atlas or Community Server

### Prerequisites

 
* Run ```npm install``` 
* If you run locally run the mongodb first using the command ```mongod``` and 
make sure to use the right connection string ```mongoose.connect("mongodb://localhost/apiQuizTest")```
* Run ```nodemon index.js``` nodemon will watch for changes on your code

### Installing

Using git

```
 git clone https://gitlab.com/cristian.cernat97/quiz-ceiti-api
```
or download the zip file

## Testing and Middleware
...soon...

## Built With

* [Nodemon](https://github.com/remy/nodemon) Watcher
* [Mocha](https://mochajs.org/) Test Framework
* [MongoDB] (https://www.mongodb.com/download-center#community) NoSQL database
* [Node](https://nodejs.org/en/) NodeJS

## Contributing

Go ahead blame me for anything I am still learning 
## Versioning

Version 1.017

## Authors

* **Cristian Cernat** - (https://gitlab.com/cristian.cernat97)